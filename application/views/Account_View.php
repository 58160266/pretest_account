<!DOCTYPE html>
<html>
<head>
	<title>Account View</title>
</head>
<body>
<table border="2">
	<tr>
		<td colspan="7">
		<a href="<?php echo base_url()?>account_control/create_account"> 
			<input type="submit" value="create" />
		</a>
		<a href="<?php echo base_url()?>account_control/deposit"> 
			<input type="submit" value="deposit" />
		</a>
		<a href="<?php echo base_url()?>account_control/withdraw"> 
			<input type="submit" value="withdraw" />
		</a>
		<a href="<?php echo base_url()?>account_control/transfer"> 
			<input type="submit" value="transfer" />
		</a>
		</td>
	</tr>
	<tr>
		<td>ACC_No</td>
		<td>ACC_Name</td>
		<td>ACC_Surname</td>
		<td>DateOp</td>
		<td>Balance</td>
		<td colspan="2">Option</td>
	</tr>
	<?php foreach ($acc as $value) { ?>
	<tr>	
		<td><?php echo $value['ACC_No']; ?></td>
		<td><?php echo $value['ACC_Name']; ?></td>
		<td><?php echo $value['ACC_Surname']; ?></td>
		<td><?php echo $value['DateOp']; ?></td>
		<td><?php echo $value['Balance']; ?></td>
		<td colspan="2">
			<a href="<?php echo base_url()?>account_control/edit_account/<?php echo $value['ACC_No'] ?>"> 
				<input type="submit" value="แก้ไข" />
			</a>
			<a href="<?php echo base_url()?>account_control/delete_account/<?php echo $value['ACC_No'] ?>"> 
				<input type="submit" value="ลบ" />
			</a>
		</td>
	</tr>
	<?php } ?>
</table>
</body>
</html>