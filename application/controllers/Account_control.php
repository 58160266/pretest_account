<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account_control extends CI_Controller {

	public function index()
	{
		$this->load->model('Account_Model');
		$data['acc'] = $this->Account_Model->list_all_account();
		$this->load->view('Account_View',$data);
	}

	public function create_account()
	{
		if($this->input->post('submit')!=NULL){
			$this->load->model('Account_Model');
			$this->Account_Model->create_account();
			redirect('Account_control');
		}else{
			$this->load->view('Account_Create');
		}
	}

	public function edit_account(){
		$this->load->model('Account_Model');
		if($this->input->post('edit')!=NULL){
			$this->Account_Model->update_account();
			redirect('Account_control');
		}else{
			$acc = $this->uri->segment(3);
			$data['acc'] = $this->Account_Model->list_one_account($acc);
			$this->load->view('Account_Edit',$data);
		}
	}
	public function delete_account()
	{
		$this->load->model('Account_Model');
		$acc = $this->uri->segment(3);
		$this->Account_Model->delete_account($acc);
		redirect('Account_control');
	}

	public function deposit()
	{
		if ($this->input->post('deposit')!=NULL) {
			$this->load->model('Account_Model');
			$this->Account_Model->deposit();
			redirect('Account_control');
		}else{
			$this->load->view('Account_Deposit');
		}
	}

	public function withdraw()
	{
		if ($this->input->post('withdraw')!=NULL) {
			$this->load->model('Account_Model');
			$this->Account_Model->withdraw();
			redirect('Account_control');
		}else{
			$this->load->view('Account_Withdraw');
		}
	}

	public function transfer()
	{
		if ($this->input->post('transfer')!=NULL) {
			$this->load->model('Account_Model');
			$this->Account_Model->transfer();
			redirect('Account_control');
		}else{
			$this->load->view('Account_Transfer');
		}
	}

}
