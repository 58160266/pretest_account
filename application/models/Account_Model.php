<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account_Model extends CI_Model {

	public function list_all_account()
	{
		$result = $this->db->get('Account');
		return $result->result_array();
	}

	public function create_account()
	{
		$data = array(
			'ACC_No' => $this->input->post("ACC_No"),
			'ACC_Name' => $this->input->post("ACC_Name"),
			'ACC_Surname' => $this->input->post("ACC_Surname"),
			'DateOp' => date('Y-m-d'), 
			'Balance' => $this->input->post("Balance")
		);

		$this->db->insert('Account',$data);
	}

	public function list_one_account($acc)
	{
		$this->db->where('ACC_No',$acc);
		$result = $this->db->get('Account');
		return $result->result_array();
	}

	public function update_account()
	{
		$data = array(
			'ACC_Name' => $this->input->post("ACC_Name"),
			'ACC_Surname' => $this->input->post("ACC_Surname"),
			'Balance' => $this->input->post("Balance")
		);
		$this->db->where('ACC_No',$this->input->post("ACC_No"));
		$this->db->update('Account',$data);
	}

	public function delete_account($acc)
	{
		$this->db->where('ACC_No',$acc);
		$this->db->delete('Account');
	}

	public function deposit()
	{
		$data = array(
			'ACC_No_Source' => $this->input->post('ACC_No'),
			'Amount' => $this->input->post('Balance'),
			'DateOp' => date('Y-m-d'),
			'ACC_Option' => 'D' 
		);
		$this->db->insert('Transfer',$data);
	}
	
	public function withdraw()
	{
		$data = array(
			'ACC_No_Source' => $this->input->post('ACC_No'),
			'Amount' => $this->input->post('Balance'),
			'DateOp' => date('Y-m-d'),
			'ACC_Option' => 'W' 
		);
		$this->db->insert('Transfer',$data);
	}

	public function transfer()
	{
		$data = array(
			'ACC_No_Source' => $this->input->post('ACC_No_Source'),
			'ACC_No_Dest' => $this->input->post('ACC_No_Dest'),
			'Amount' => $this->input->post('Balance'),
			'DateOp' => date('Y-m-d'),
			'ACC_Option' => 'T' 
		);
		$this->db->insert('Transfer',$data);
	}
}
